import React from 'react';
import './TodoApp.css';
import axios from "axios";
import Login from "./Login";

class TaskComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            tasks: [],
            name: '',
            edit: 0,
        };
    }

    componentDidMount() {
        axios.get('http://localhost:3004/usertasks', {params: {userId: this.props.userId}})
            .then((response) => {
                this.setState({tasks: response.data.todoTasks});
            })
    }

    addItems = () => {
        axios.post('http://localhost:3004/usertasks', {task: this.state.name, userId: this.props.userId})
            .then((response) => {
                this.setState({tasks: response.data.todoTasks})
            });
    }
    passName = (e) => {
        this.setState({name: e.target.value});
    }

    editItems = (sno) => {
        //console.log(sno);
        this.setState({edit: 1})
    }

    editContent = (e) => {
        this.setState({name: e.target.value});
    }

    confirmEdit = (sno) => {
        axios.put('http://localhost:3004/update/', {
            body: {
                content: this.state.name,
                d_id: sno,
                userId: this.props.userId
            }
        })
            .then((response) => {
                this.setState({edit: 0});
                this.setState({tasks: response.data.todoTasks});
            });
    }
    deleteItems = (sno) => {

        axios.delete('http://localhost:3004/remove/', {data: {d_id: sno, userId: this.props.userId}}
        ).then((response) => {
            this.setState({tasks: response.data.todoTasks})
            //console.log((this.state.tasks[0]).content);
        });
    }

    render() {
        return (
            <>
                {(this.props.isLogin) ?
                    (<div className={"main"}>
                        <div className={"userName"}> Welcome {this.props.firstName} </div>
                        <div className={"todoContainer"}>
                            <h2>TODO-LIST</h2>
                            <div className={"form"}>
                                <form>
                                    <label>
                                        <span className="Enter">Enter Tasks </span>
                                        <input type="text" className="textField" name="name" onChange={this.passName}/>
                                    </label>
                                    <input type="button" value="Add" className="AddButton" onClick={this.addItems}/>
                                </form>
                            </div>
                            <ul className="todo-list">
                                {this.state.edit === 0 ?
                                    this.state.tasks.map((item, index) => {
                                        return <li>
                                            <div className="todo-list-item-name">{item.content}</div>
                                            <div className="todoButtons">
                                                <button type="button" className="EditButton"
                                                        onClick={this.editItems}>Edit
                                                </button>
                                                <button type="button" className="DeleteButton"
                                                        onClick={() => this.deleteItems(item._id)}>Delete
                                                </button>
                                            </div>
                                        </li>
                                    }) : this.state.tasks.map((item, index) => {
                                        return <li>
                                            <div className="todo-list-item-name">
                                                <form>
                                                    <input className="editField" type="text" defaultValue={item.content}
                                                           name="content"
                                                           onChange={this.editContent}/>
                                                    <button type="submit"
                                                            onClick={() => this.confirmEdit(item._id)}>Confirm
                                                    </button>
                                                    <a href="/" className="cancel">Cancel</a>
                                                </form>
                                            </div>
                                        </li>
                                    })
                                }
                            </ul>
                        </div>
                    </div>) : (alert('User Must be Logged in First '))}
            </>);
    }
}
export default TaskComponent;
