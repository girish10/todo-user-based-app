import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';
import RaisedButton from 'material-ui/RaisedButton'
import TextField from 'material-ui/TextField';
import {Component} from "react";
import React from "react";
import './App.css'
import axios from "axios";
import TaskComponent from "./TodoTasks";
class Login extends Component {

    constructor(props) {
        super(props);
        this.state={
            username: '',
            password: '',
            isLogin: false,
            firstName :'',
            userId : ''
        }
    }
    changeUserName = (e) => {
        this.setState({username: e.target.value});
    }

    changePassword = (e) => {
        this.setState({password: e.target.value});
    }

    handleClick = (e) => {
        if(this.state.username === '' || this.state.password === '') {
            alert('All Fields Are Mandatory');
        }
        else {
            console.log(this.state.username, this.state.password);
            const payload = {
                email: this.state.username,
                password: this.state.password
            }
            axios.post('http://localhost:3004/login', payload)
                .then((response) => {
                    if (response.data.isLogin === true) {
                        alert("Login Successful");
                        this.setState({isLogin:true,
                            userId: response.data.user_details[0]._id,
                            firstName: response.data.user_details[0].firstName}
                        );
                    }
                    else {
                        alert(response.data.message);
                    }
                })
        }
    }
    render() {
        return (<>
                {this.state.isLogin ? <TaskComponent userId={this.state.userId} firstName={this.state.firstName} isLogin={this.state.isLogin} /> :
                (<div className={"LoginForm"}>
                    <h3>Login Form </h3>
                    <MuiThemeProvider>
                        <div>
                            <AppBar title="Login" className={"LoginBar"}/>
                            <TextField hintText={"Enter Your Registered Email"} floatingLabelText={"Username"} onChange = {this.changeUserName} />
                            <br/>
                            <TextField hintText={"Enter Your Password"} floatingLabelText={"Password"} onChange = {this.changePassword} />
                            <br/>
                            <RaisedButton label={"Login"} primary={true} onClick={this.handleClick} />
                        </div>
                    </MuiThemeProvider>
                </div>)}
                </>);
    }
}
export default Login;