import MultiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import {Component} from "react";
import React from "react";
import axios from "axios";
import {MuiThemeProvider} from "material-ui";
import Popup from "reactjs-popup";
import Login from "./Login";
class Register extends Component {
    constructor(props) {
        super(props);
        this.state = {
            firstName :'',
            lastName :'',
            email: '',
            phoneNumber:'',
            password:'',
            confirmPassword:'',
            isRegistered : false,
        }
    }
    handleFirstName= (e) => {
        this.setState({firstName: e.target.value});
    }
    handleLastName= (e) => {
        this.setState({lastName: e.target.value});
    }
    handleEmail= (e) => {
        this.setState({email: e.target.value});
    }
    handlePassword = (e) => {
        this.setState({password: e.target.value});
    }
    handleConfirmPassword = (e) => {
        this.setState({confirmPassword: e.target.value});
    }
    handlePhoneNumber = (e) => {
        this.setState({phoneNumber:e.target.value})
    }
    handleRegistration = (e) => {
        if (this.state.firstName === '' || this.state.lastName === '' || this.state.email === '' || this.state.phoneNumber === '' || this.state.password === '' || this.state.confirmPassword === '') {
            alert('All Fields Are Mandatory');
        } else if (this.state.password !== this.state.confirmPassword) {
            alert('Password didn\'t Matched');
        } else if (((this.state.phoneNumber).length) < 10 || ((this.state.phoneNumber).length) > 10) {
            alert('Phone Number must be  of 10 digits');
        } else if (this.state.password.length < 6) {
            alert('Password length must be greater then 6');
        } else {
            console.log(this.state.email, this.state.firstName, this.state.lastName, this.state.email, this.state.phoneNumber, this.state.password);
            const payload = {
                firstName: this.state.firstName,
                lastName: this.state.lastName,
                email: this.state.email,
                phoneNumber: this.state.phoneNumber,
                password: this.state.password
            }
            axios.post('http://localhost:3004/register', payload)
                .then((response) => {
                    alert(response.data.message);
                    console.log(response.data.message);
                    if (response.data.code === 200) {
                        this.setState({isRegistered: true});
                    }
                });
        }
    }
    render() {
       return(
           <>
           {(this.state.isRegistered) ? (<Login/>):
               (<div className={"RegistrationForm"}>
                   <h2>User Registration Form</h2>
                   <MuiThemeProvider>
                       <div>
                           <AppBar title="Register"/>
                           <TextField hintText={"Enter Your First Name"} floatingLabelText={"First Name"}
                                      onChange={this.handleFirstName}/><br/>
                           <TextField hintText={"Enter Your Last Name"} floatingLabelText={"Last Name"}
                                      onChange={this.handleLastName}/><br/>
                           <TextField hintText={"Enter Your Email"} floatingLabelText={"Email"}
                                      onChange={this.handleEmail}/><br/>
                           <TextField hintText={"PhoneNumber"} floatingLabelText={"PhoneNumber"}
                                      onChange={this.handlePhoneNumber}/><br/>
                           <TextField hintText={"Enter Your Password"} floatingLabelText={"Password"}
                                      onChange={this.handlePassword}/><br/>
                           <TextField hintText={"Enter Your Password"} floatingLabelText={"Confirm Password"}
                                      onChange={this.handleConfirmPassword}/><br/>
                           <RaisedButton label={"Register"} primary={true} onClick={this.handleRegistration}/>
                       </div>
                   </MuiThemeProvider>
               </div>)}
            </> );
    }
}
export default Register;