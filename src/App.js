import React, {Component} from 'react';
import './App.css';
import {BrowserRouter as Router, Link, Route, Switch} from "react-router-dom";
import Login from "./Login";
import Register from "./Register";
import TaskComponent from './TodoTasks';
class App extends Component {
  render(){
    return (
            <Router>
                <div className={"navbar"}>
                    <Link to={"/"}>Xencov</Link>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <Link to={"/"}>Home</Link>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <Link to={"/login"}>Login</Link>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <Link to={"/register"}>Register</Link>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <Link to={"/tasks"}>My Tasks</Link>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <Link to={"/"}>Logout</Link>
                </div>
                <div className={"Main"}>
                    <h2>Welcome To Do App</h2>
                    <Switch>
                        <Route path={"/login"}><Login /></Route>
                        <Route path={"/register"}><Register /></Route>
                        <Route path={"/tasks"}><TaskComponent /></Route>
                    </Switch>
                </div>
            </Router>);
  }
}
export default App;
